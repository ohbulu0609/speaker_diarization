# Speaker Diarization

Speaker diarization mainly contians two part for which [UTTERANCE-LEVEL AGGREGATION FOR SPEAKER RECOGNITION IN THE WILD](https://arxiv.org/pdf/1902.10107.pdf) and [FULLY SUPERVISED SPEAKER DIARIZATION](https://arxiv.org/pdf/1810.04719.pdf).
This project mainly borrowed from githubs [UIS-RNN](https://github.com/google/uis-rnn), [VGG-Speaker-recognition](https://github.com/WeidiXie/VGG-Speaker-Recognition) and [Kersa-Speaker-Recognition](https://github.com/yeyupiaoling/Kersa-Speaker-Recognition)

### Requirements

see [requirements.txt](requirements.txt)

### Datasets

#### Download link (Google drive)
https://drive.google.com/drive/folders/1qp45YR7GA6KkYbX8SMzCuv0WIKY7G5WZ?usp=sharing

English
 - [VCTK](https://datashare.is.ed.ac.uk/handle/10283/2651) : contains 109 speakers of English.
 - [VoxCeleb1](http://www.robots.ox.ac.uk/~vgg/data/voxceleb/vox1.html) : contains 1251 speakers.
 - [VoxCeleb2](http://www.robots.ox.ac.uk/~vgg/data/voxceleb/vox2.html)： contains 6112 speakers.

Chinese

 - [Aishell](http://www.openslr.org/resources/33) : 400 people from different accent areas in China are invited to participate in the recording
 - [Free ST-Chinese-Mandarin-Corpus](http://www.openslr.org/resources/38) : It has 855 speakers. Each speaker has 120 utterances.
 - [THCHS-30](http://www.openslr.org/resources/18) : an open Chinese speech database published by Center for Speech and Language Technology (CSLT) at Tsinghua University.
 - [aidatatang_200zh](http://www.openslr.org/resources/62) : The corpus contains 200 hours of acoustic data, which 600 speakers from different accent areas in China are invited to participate in the recording.
 - [CN-Celeb](http://www.openslr.org/resources/82) : The dataset contains more than 130,000 utterances from 1,000 Chinese celebrities

### Train embedding model (ghostvlad)

1. create train_list.txt and test_list.txt

    ```bash
    python create_data.py
    ```

2. ```cd VGG-Speaker-Recognition/src``` and train embedding model

    ```bash
    python main.py --gpu 0
    ```

3. ```cd VGG-Speaker-Recognition/src``` calculate EER

    ```bash
    python predict.py --gpu 0
    ```

### Generate embedding vectors

1. ```cd Speaker-Diarization/ghostvlad``` and generate embedding vectors (with pretrain model in ``` Speaker-Diarization/ghostvlad/pretrained/weights.h5``` or the model trained in last section)

    training data

    ```bash
    python generate_embeddings_train.py --gpu 0
    ```

    testing data

    ```bash
    python generate_embeddings_test.py --gpu 0
    ```

    * Note: ghost_cluster, vlad_cluster, n_classes, for epoch in range(<10>) have to be identified to the model we use

### Train diarization model (uis-rnn)

1. ```cd uis-rnn``` and train uis-rnn model

    ```bash
    python demo.py
    ```

    testing_data.npz Accuracy:
    ![accuracy](acc.jpg)

### Inference

1. environment setup on computer with sound card
    1. conda create --name speakerdiarization
    2. conda activate speakerdiarization
    3. conda install -c conda-forge keras
    4. conda install pytorch torchvision torchaudio cudatoolkit=10.2 -c pytorch
    5. download pyaudio [PyAudio-0.2.11-cp37-cp37m-win_amd64.whl](https://github.com/intxcc/pyaudio_portaudio) 
    6. pip install /path/to/PyAudio-0.2.11-cp37-cp37m-win_amd64.whl
    7. download [SoundFile-0.10.3.post1.tar.gz](https://github.com/bastibe/SoundFile/releases/tag/0.10.3post1)
    8. pip install /path/to/SoundFile-0.10.3.post1.tar.gz


2. ```cd Speaker-Diarization``` and diarization of a wav file

    ```bash
    python speakerDiarization.py
    ```

### Baseline


### Test result

[inews_Taiwan.wav](https://www.youtube.com/watch?v=tF09J283quc)

groud_ture

| 0~5     |5~9     |9~19   |19~31|31~44|44~51|51~61|61~69|69~86|86~100|100~116|
| --------------| -------------|------------|---------|----------|--------|---------|---------|---------|---------|---------|
| silence       |English anchor|inews anchor|陳建仁(英)|inews anchor|English anchor|inews anchor|Stacy(英)|inews anchor|Stacy(中)|inews anchor|

diarization result
![inews_Taiwan](dizrization_e1.2_o_0.4.jpg)

### Reference

[VGG-Speaker-Recognition](https://github.com/WeidiXie/VGG-Speaker-Recognition)

[UIS-RNN](https://github.com/google/uis-rnn)

[Speaker-Diarization](https://github.com/taylorlu/Speaker-Diarization)

[Kersa-Speaker-Recognition](https://github.com/yeyupiaoling/Kersa-Speaker-Recognition)
