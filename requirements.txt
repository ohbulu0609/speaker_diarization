python==3.7
tensorflow-gpu==1.14.0
Keras==2.2.5
cudatoolkit==10.0
cudnn==7.6.5
pytorch==1.4.0
librosa
PyAudio
