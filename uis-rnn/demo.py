# Copyright 2018 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""A demo script showing how to use the uisrnn package on toy data."""

import numpy as np

import uisrnn

import sys
import argparse

def diarization_experiment(model_args, training_args, inference_args, demo_args):
  """Experiment pipeline.

  Load data --> train model --> test model --> output result

  Args:
    model_args: model configurations
    training_args: training configurations
    inference_args: inference configurations
    demo_args: demo configurations
  """

  model = uisrnn.UISRNN(model_args)

  # Training.
  # If we have saved a mode previously, we can also skip training by
  # calling：
  # model.load(SAVED_MODEL_NAME)
  if demo_args.mode == 'train_test' or demo_args.mode == 'train':
    train_data = np.load('/home/ohblue/桌面/vox1/Speaker-Diarization/ghostvlad/training_data.npz', allow_pickle=True)
    train_sequence = train_data['train_sequence']
    train_sequence = train_sequence + sys.float_info.epsilon
    print('sys.float_info.epsilon: ', sys.float_info.epsilon)
    train_cluster_id = train_data['train_cluster_id']
    print('type train_sequence: ', type(train_sequence), type(train_sequence[0][0]))
    train_sequence = train_sequence.astype(float)
    model.fit(train_sequence, train_cluster_id, training_args)
    model.save(demo_args.saved_model_path)

  # Testing.
  # You can also try uisrnn.parallel_predict to speed up with GPU.
  # But that is a beta feature which is not thoroughly tested, so
  # proceed with caution.
  if demo_args.mode == 'train_test' or demo_args.mode == 'test':
    predicted_cluster_ids = []
    test_record = []
    test_data = np.load('/home/ohblue/桌面/vox1/Speaker-Diarization/ghostvlad/testing_data.npz', allow_pickle=True)
    test_sequences = test_data['test_sequences']
    test_sequences = [seq.astype(float) for seq in test_sequences]
    test_cluster_ids = test_data['test_cluster_ids'].tolist()
    model.load(demo_args.saved_model_path)
    for (test_sequence, test_cluster_id) in zip(test_sequences, test_cluster_ids):
      predicted_cluster_id = model.predict(test_sequence, inference_args)
      predicted_cluster_ids.append(predicted_cluster_id)
      accuracy = uisrnn.compute_sequence_match_accuracy(
          test_cluster_id, predicted_cluster_id)
      test_record.append((accuracy, len(test_cluster_id)))
      print('Ground truth labels:')
      print(test_cluster_id)
      print('Predicted labels:')
      print(predicted_cluster_id)
      print('-' * 80)
    output_string = uisrnn.output_result(model_args, training_args, test_record)
    print('Finished diarization experiment')
    print(output_string)

def main():
  """The main function."""
  model_args, training_args, inference_args, demo_args = uisrnn.parse_arguments()
  model_args.observation_dim = 512
  demo_args.saved_model_path = '/home/ohblue/桌面/vox1/Speaker-Diarization/pretrained/saved_model.uisrnn_benchmark'
  print('model_args.observation_dim: ', model_args.observation_dim)
  print('demo_args.mode: ', demo_args.mode)
  print('demo_args.saved_model_path: ', demo_args.saved_model_path)
  diarization_experiment(model_args, training_args, inference_args, demo_args)

if __name__ == '__main__':
  main()
