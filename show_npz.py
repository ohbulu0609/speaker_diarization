import numpy as np
import argparse
import os

def main(args):
    # scaling_npz_stub = os.path.join(wham_noise_path, 'metadata', 'scaling_{}.npz')
    # scaling_npz_path = '/workspace/Liam/wham/wham_noise/metadata/scaling_tt.npz'
    scaling_npz_path = os.path.join(args.path)
    scaling_npz = np.load(scaling_npz_path, allow_pickle=True)
    for i in scaling_npz.files:
        print(i)
        print('='*80)
        print(scaling_npz[i])
        print('len of array: ', len(scaling_npz[i]))
        print('type of array: ', type(scaling_npz[i]))
        if isinstance(scaling_npz[i], np.ndarray):
            print('shape of array: ', scaling_npz[i].shape)
        print(scaling_npz[i][0])
        print('len of array of 0: ', len(scaling_npz[i][0]))
        print('type of array of 0: ', type(scaling_npz[i][0]))
        if isinstance(scaling_npz[i][0], np.ndarray):
            print('shape of array of 0: ', scaling_npz[i][0].shape)
        # print('max: ', np.max(scaling_npz[i]))
        # print('min: ', np.min(scaling_npz[i]))
        print('\n')

if __name__ == "__main__":
    parser = argparse.ArgumentParser("show npz")
    parser.add_argument('--path', type=str, default='/workspace/Liam/wham/wham_noise/metadata',
                        help='clean voice directory')
    args = parser.parse_args()
    print(args)
    main(args)